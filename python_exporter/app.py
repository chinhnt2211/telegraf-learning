import requests
import time
from flask import Flask, request, redirect, render_template
from prometheus_client import Counter, generate_latest, CONTENT_TYPE_LATEST, GC_COLLECTOR, PLATFORM_COLLECTOR, PROCESS_COLLECTOR
from prometheus_client.core import CounterMetricFamily, GaugeMetricFamily, REGISTRY
from enum_log_level import LogLevel

# create and configure the app
app = Flask(__name__, instance_relative_config=True)
array_metric_family = []

API_URL="http://10.1.0.7:80/loki/api/v1/"
NAMESPACE = "telegraf"

REGISTRY.unregister(GC_COLLECTOR)
REGISTRY.unregister(PLATFORM_COLLECTOR)
REGISTRY.unregister(PROCESS_COLLECTOR)

class TelegrafCollector(object):
    log_level = ""
    labels = []
    label_values = []
    quantity = 0
    query = ""
    pramameter = ""
    

    def __init__(self, labels, label_values, interval, log_level):
        self.labels = labels
        self.label_values = label_values
        self.interval = interval
        self.log_level = log_level

        self.handle_pramameter()

    def handle_pramameter(self):
        self.query = self.query + "{"
        for i in range(0, len(self.labels)):
            self.query = self.query + self.labels[i] + " = "
            if (i <= len(self.label_values)):
                self.query = self.query + '"'+ self.label_values[i] + '",'
            else:
                self.query = self.query + '"",'
        self.query = self.query[:-1] + "}" + '|="telegraf"'

        # Filter theo level log
        match (self.log_level):
            case LogLevel.DEBUG.value:
                self.query = self.query + '|= "D!"'
            case LogLevel.INFO.value:
                self.query = self.query + '|= "I!"'
            case LogLevel.WARN.value:
                self.query = self.query + '|= "W!"'
            case LogLevel.ERROR.value:
                self.query = self.query + '|= "E!"'
            case _:
                self.query = self.query + '|= "" '

        if(self.interval != 0):
            start = int(time.time()) - int(self.interval)
            self.pramameter = "query=" + self.query + "&" + "start=" + str(start)
        else:
            self.pramameter = "query=" + self.query 


        self.pramameter = self.pramameter + "&limit=5000"

    def update_metrics(self):
        self.quantity = 0

        url = API_URL + "query_range?" + self.pramameter
        data = requests.get(url=url).json()

        self.quantity = data["data"]["stats"]["summary"]["totalEntriesReturned"]


    def collect(self):
        metrics = CounterMetricFamily("{}_{}_in_{}_seconds_total".format(
            NAMESPACE, self.log_level.lower(), self.interval), "Numbers of log level in a period", labels=["level"] +  self.labels, unit="quantity")

        metrics.add_metric(labels=[self.log_level] + self.label_values , value=self.quantity)
        yield metrics


@app.route("/add-metrics", methods = ['GET','POST'])
def add_metrics():
    if request.method == 'GET':
        return render_template('add-metrics.html')
    if request.method == 'POST':
        labels = request.form['label_names']
        label_values = request.form['label_values']
        log_level = request.form['log_level']
        interval = request.form['interval']

        labels = labels.split(",")
        label_values = label_values.split(",")
        interval = int(interval)

        array_metric_family.append(TelegrafCollector(labels, label_values, interval, log_level))
        REGISTRY.register(array_metric_family[-1])

        return redirect('/metrics')
@app.route("/metrics")
def metrics():
    for metric_family in array_metric_family :
        metric_family.update_metrics()
    data = generate_latest()
    return data, 200, {"Content-Type": CONTENT_TYPE_LATEST}




