from enum import Enum

class LogLevel(Enum):
    TRACE   = "TRACE"
    DEBUG   = "DEBUG"
    INFO    = "INFO"
    WARN    = "WARN"
    ERROR   = "ERROR"
    FATAL   = "FATAL"